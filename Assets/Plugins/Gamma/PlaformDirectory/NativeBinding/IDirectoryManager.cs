﻿using UnityEngine;
using System.Collections;

namespace Gamma.PlaformDirectory.NativeBinding {
	interface IDirectoryManager {
		string documentsDirectory {get;}
		string cachesDirectory {get;}
		string libraryDirectory {get;}
		string tempDirectory {get;}
		string installDirectory {get;}
		string applicationSupportDirectory {get;}

		void ExcludeFromPlatformCloud(string path, bool exclude);
	}
}
