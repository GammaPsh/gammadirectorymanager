﻿using UnityEngine;
using System.Collections;
using System.IO;

namespace Gamma.PlaformDirectory.NativeBinding {

	public enum AndroidFileLocation {
		Internal,
		External
	}

	public class AndroidDirectoryManager : IDirectoryManager {

		public AndroidFileLocation androidFileLocation;

		public AndroidDirectoryManager()
		{
			androidFileLocation = AndroidFileLocation.Internal;
		}

		#region IDirectoryManager implementation
		public string documentsDirectory {
			get {
				switch(androidFileLocation){
				case AndroidFileLocation.Internal:
					return Path.Combine(Application.persistentDataPath ,"Documents");
				default:
					return "/storage/sdcard0/AircraftVR/Documents";
				}

			}
		}

		public string cachesDirectory {
			get {
				return Path.Combine(libraryDirectory, "Caches");
			}
		}

		public string libraryDirectory {
			get {
				switch(androidFileLocation){
				case AndroidFileLocation.Internal:
					return Path.Combine(Application.persistentDataPath, "Library");
				default:
					return "/storage/sdcard0/AircraftVR/Library";
				}
			}
		}

		public string tempDirectory {
			get {
				switch(androidFileLocation){
				case AndroidFileLocation.Internal:
					return Path.Combine(Application.persistentDataPath, "Temp");
				default:
					return "/storage/sdcard0/AircraftVR/Temp";
				}
			}
		}

		public string installDirectory {
			get {
				return Application.dataPath;
			}
		}

		public string applicationSupportDirectory {
			get {
				return Path.Combine(libraryDirectory, "ApplicationSupport");
			}
		}

		public void ExcludeFromPlatformCloud(string path, bool excluded)
		{
			Debug.LogWarning("No Cloud settings for Android set");
		}
		#endregion



	}
}
