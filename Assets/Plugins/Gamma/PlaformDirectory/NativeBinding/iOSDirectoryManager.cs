﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace Gamma.PlaformDirectory.NativeBinding {
	
	public class iOSDirectoryManager : IDirectoryManager {
		
		#region IDirectoryManager implementation
		public string documentsDirectory {
			get{
				return System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
			}
		}
		
		public string cachesDirectory {
			get {
				return _getCachesPath();
			}
		}
		
		public string libraryDirectory {
			get {
				return _getLibraryPath();
			}
		}
		
		public string tempDirectory {
			get {
				return _getTempPath();
			}
		}

		public string applicationSupportDirectory {
			get {
				return _getAppSupportPath();
			}
		}

		public string installDirectory {
			get {
				return documentsDirectory.Remove(documentsDirectory.LastIndexOf("/"));
			}
		}

		public void ExcludeFromPlatformCloud(string path, bool excluded)
		{
			_excludeFromCloud(path, excluded);
		}
		#endregion

		[DllImport("__Internal")]
		private static extern string _getCachesPath();
		
		[DllImport("__Internal")]
		private static extern string _getLibraryPath();
		
		[DllImport("__Internal")]
		private static extern string _getTempPath();

		[DllImport("__Internal")]
		private static extern string _getAppSupportPath();

		[DllImport("__Internal")]
		private static extern string _excludeFromCloud(string path, bool excluded);
	}
}
