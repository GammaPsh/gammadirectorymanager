﻿using UnityEngine;
using System.Collections;
using System.IO;

namespace Gamma.PlaformDirectory {
	public static class DirectoryManager {

		private static NativeBinding.IDirectoryManager platformSpecific;
		static DirectoryManager()
		{
	#if UNITY_EDITOR
			platformSpecific = new NativeBinding.EditorDirectoryManager();
	#elif UNITY_IPHONE
			platformSpecific = new NativeBinding.iOSDirectoryManager();
	#elif UNITY_ANDROID
			platformSpecific = new NativeBinding.AndroidDirectoryManager();
	#elif UNITY_STANDALONE
			platformSpecific = new NativeBinding.StandaloneDirectoryManager();
	#endif
		}

		public static void ExcludeFromPlatformCloud(string path, bool excluded)
		{
			platformSpecific.ExcludeFromPlatformCloud(path, excluded);
		}

		public static string documentsDirectory {
			get {
				return CreateDirectoryIfNotExists(platformSpecific.documentsDirectory);
			}
		}
		public static string cachesDirectory {
			get {
				return CreateDirectoryIfNotExists(platformSpecific.cachesDirectory);
			}
		}
		public static string libraryDirectory {
			get {
				return CreateDirectoryIfNotExists(platformSpecific.libraryDirectory);
			}
		}
		public static string tempDirectory {
			get {
				return CreateDirectoryIfNotExists(platformSpecific.tempDirectory);
			}
		}

		public static string installDirectory {
			get{
				return CreateDirectoryIfNotExists(platformSpecific.installDirectory);
			}
		}

		public static string applicationSupportDirectory {
			get {
				return CreateDirectoryIfNotExists(platformSpecific.applicationSupportDirectory);
			}
		}

		public static string saveDirectory {
			get{
				return CreateSubDirectoryIfNotExists(platformSpecific.applicationSupportDirectory, "saveData");
			}
		}

		public static string CreateSubDirectoryIfNotExists(string path, string subdirectory)
		{
			string temp = Path.Combine(path, subdirectory);
			CreateDirectoryIfNotExists(temp);

			return temp;
		}

		public static string CreateDirectoryIfNotExists(string path)
		{
			if(!Directory.Exists(path))
				Directory.CreateDirectory(path);

			return path;
		}
	}
}